package AirlineReservation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class BookingDAO {

	public void addBooking(Booking booking) throws SQLException {
		String query = "Insert into booking values(?,?,?,?,?,?,?,?,?)";
		Connection con = DBConnection.getConnection();
		java.sql.Date dob = new java.sql.Date(booking.dob.getTime());
		java.sql.Date tdate = new java.sql.Date(booking.travelDate.getTime());
		PreparedStatement pst = con.prepareStatement(query);

		pst.setString(1, booking.pid);
		pst.setString(2, booking.pname);
		pst.setDate(3, dob);
		pst.setString(4, booking.address);
		pst.setString(5, booking.idproof);
		pst.setString(6, booking.flightNo);
		pst.setString(7, booking.from);
		pst.setString(8, booking.dest);
		pst.setDate(9, tdate);

		pst.executeUpdate();
		con.close();
		System.out.println("==================================================");
		System.out.println("Ticket Booked successful for  : " + booking.pname);
		System.out.println("==================================================");

	}

	public void cancelBooking(String pid) throws SQLException {

		String query = "delete from booking where pid = ?;";
		Connection con = DBConnection.getConnection();
		PreparedStatement pst = con.prepareStatement(query);
		pst.setString(1, pid);
		pst.executeUpdate();
		con.close();
		System.out.println("==================================================");
		System.out.println("Ticket cancellation successful for pid : " + pid);
		System.out.println("==================================================");
	}

	public void getPassengersList() throws SQLException {
		String query = "select * from booking;";
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			System.out.println("---------------------------------------------------------------------------");
			System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getDate(3) + " " + rs.getString(4)
					+ " " + rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7) + " " + rs.getString(8)
					+ " " + rs.getDate(9));
		}
		System.out.println("---------------------------------------------------------------------------");
		rs.close();
		con.close();
	}

	public int getBookedCount(String fno, java.util.Date date) throws SQLException {

		String query = "select count(pname) from booking where flightNo=? and travelDate=?";
		Connection con = DBConnection.getConnection();
		PreparedStatement pst = con.prepareStatement(query);
		java.sql.Date sqldate = new java.sql.Date(date.getTime());
		pst.setString(1, fno);
		pst.setDate(2, sqldate);
		ResultSet rs = pst.executeQuery();
		rs.next();
		int row = rs.getInt(1);
		rs.close();
		con.close();
		return row;

	}

	public void updatePassenger(String pid, Date travelDate) throws SQLException {
		String query = "update booking set travelDate = ? where pid = ?";
	    try {
	        Connection con = DBConnection.getConnection();
	        java.sql.Date sqldate = new java.sql.Date(travelDate.getTime());
	        PreparedStatement pst = con.prepareStatement(query);
	        pst.setDate(1, sqldate);
	        pst.setString(2, pid);
	        int rowsAffected = pst.executeUpdate();
	        if (rowsAffected == 0) {
	            System.out.println("Passenger not available.");
	        } else {
	            System.out.println("==================================================");
	            System.out.println("Passenger Details updated successfully.");
	            System.out.println("==================================================");
	        }
	        con.close();
	    } catch (SQLException e) {
	        System.out.println("An error occurred while updating passenger details: " + e.getMessage());
	    }
	}

}
