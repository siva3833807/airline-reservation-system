package AirlineReservation;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class AirlineReservationSystem {

	public static void main(String[] args) throws SQLException {

		Scanner scan = new Scanner(System.in);
		System.out.println("#################################################");
		System.out.println("#\t    AIRLINE RESERVATION SYSTEM   \t#");
		System.out.println("#################################################");
		System.out.println("1. Passenger \n2. Admin \n");
		int n = scan.nextInt();

		if (n == 1) {
			passengerConsole(scan);
		} else if (n == 2) {
			adminConsole(scan);
		} else {
			System.out.println("Enter correct option .");
		}

	}

//	Admin
	public static void adminConsole(Scanner scan) throws SQLException {
		int y = 0;
		do {
			System.out.println(
					"**** Admin Console **** \n1. Add Flight \n2. Remove Flight \n3. Get Flight Details  \n4. Update Flight Details \n5. Get Passengers Detail \n6. Exit\n");
			y = scan.nextInt();
			scan.nextLine();
			switch (y) {
			case 1: {
				addFlight();
				break;
			}
			case 2: {
				removeFlight(scan);
				break;
			}
			case 3: {
				getFlightDetails();
				break;
			}
			case 4: {
				updateFilght(scan);
				break;
			}
			case 5: {
				getPassengerlist();
				break;
			}
			case 6: {
				System.out.println("Exitting.......");
				return;
			}
			default: {
				System.out.println("Enter correct option .");
				break;
			}
			}
		} while (y != 6);
	}

//	Passenger
	public static void passengerConsole(Scanner scan) throws SQLException {
		int x = 0;
		do {
			System.out.println(
					"**** Passenger Console **** \n1. Book Ticket \n2. Cancel Ticket \n3. Get Flight Details \n4. Update Passenger Details \n5. Exit\n");
			x = scan.nextInt();
			scan.nextLine();

			BookingDAO bdao = new BookingDAO();
			switch (x) {
			case 1: {
				bookTicket(bdao);
				break;
			}
			case 2: {
				cancelTicket(bdao, scan);
				break;
			}
			case 3: {
				availableSeats();
				break;
			}
			case 4: {
				updatePassenger(scan);
				break;
			}
			case 5: {
				System.out.println("Exitting........");
				return;
			}
			default: {
				System.out.println("Enter correct option. \n");
				break;
			}
			}
		} while (x != 5);
	}


// Admin Console Methods	

	private static void addFlight() throws SQLException {
		Flight flight = new Flight();
		FlightDAO fdoa = new FlightDAO();
		fdoa.addFlight(flight);

	}

	private static void removeFlight(Scanner scan) throws SQLException {
		System.out.println("Enter Flight id to remove flight : ");
		String fid = scan.next();
		FlightDAO fdoa = new FlightDAO();
		fdoa.removeFlight(fid);

	}

	private static void updateFilght(Scanner scan) throws SQLException {
		FlightDAO fdoa = new FlightDAO();
		System.out.println("Enter flight id to update a flight : ");
		String fid = scan.next();
		System.out.println("Enter Capacity : ");
		int capacity = scan.nextInt();
		fdoa.updateFlightDetails(fid, capacity);
	}

	private static void getPassengerlist() throws SQLException {
		BookingDAO bdao = new BookingDAO();
		bdao.getPassengersList();
	}

	private static void getFlightDetails() throws SQLException {
		FlightDAO fdoa = new FlightDAO();
		fdoa.getFlightsDetails();

	}

//	Passenger Console

	private static void bookTicket(BookingDAO bdao) throws SQLException {
		Booking booking = new Booking();
		if (booking.isAvailable()) {
			bdao.addBooking(booking);
		} else {
			System.out.println("Flight is not available");
		}

	}

	private static void cancelTicket(BookingDAO bdao, Scanner scan) throws SQLException {
		System.out.println("Enter the passenger id to cancel ticket : ");
		String pid = scan.next();
		bdao.cancelBooking(pid);
	}

	private static void availableSeats() throws SQLException {
		FlightDAO fdoa = new FlightDAO();
		fdoa.getAvailableSeats();

	}

	private static void updatePassenger(Scanner scan) throws SQLException {
		try {
		System.out.println("Enter Passenger id to update a flight : ");
		String pid = scan.next();
		System.out.println("Update Date : ");
		String tdate = scan.next();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		Date travelDate = dateFormat.parse(tdate);
		BookingDAO bdoa = new BookingDAO();
		bdoa.updatePassenger(pid, travelDate);

		} catch (ParseException e) {

		System.out.println("Passenger not availabe");
		}

	}


}