package AirlineReservation;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Booking {

	String pid;
	String pname;
	Date dob;
	String address;
	String idproof;
	String flightNo;
	String from;
	String dest;
	Date travelDate;

	Booking() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter passenger id : ");
		pid = sc.next();
		System.out.println("Enter passenger name : ");
		pname = sc.next();
		System.out.println("Enter passenger dob (dd-mm-yyyy) : ");
		String pdob = sc.next();
		System.out.println("Enter passenger address : ");
		address = sc.next();
		System.out.println("Enter passenger Aadhaar : ");
		idproof = sc.next();
		System.out.println("Enter passenger flight number : ");
		flightNo = sc.next();
		System.out.println("Enter passenger from : ");
		from = sc.next();
		System.out.println("Enter passenger destination : ");
		dest = sc.next();
		System.out.println("Enter date of travel (dd-mm-yyyy) : ");
		String tdate = sc.next();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		try {

			dob = dateFormat.parse(pdob);
			travelDate = dateFormat.parse(tdate);

		} catch (ParseException e) {

			e.printStackTrace();
		}
		sc.close();
	}

	public boolean isAvailable() throws SQLException {

		FlightDAO flightdao = new FlightDAO();
		BookingDAO bookingdao = new BookingDAO();
		int capacity = flightdao.getCapacity(flightNo);

		int booked = bookingdao.getBookedCount(flightNo, travelDate);

		return booked < capacity;

	}
}
