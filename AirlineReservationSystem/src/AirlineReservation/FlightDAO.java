package AirlineReservation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FlightDAO {

	public void addFlight(Flight flight) throws SQLException {
		
		Connection con = DBConnection.getConnection();
		String query = "insert into flight values(?,?,?,?);";
		PreparedStatement pst = con.prepareStatement(query);
		
		pst.setString(1, flight.fid);
		pst.setString(2, flight.fname);
		pst.setBoolean(3, flight.fclass);
		pst.setInt(4, flight.fcapacity);
		pst.executeUpdate();
		con.close();
		System.out.println("**************************");
		System.out.println("Flight Added successfully");
		System.out.println("**************************");
	}
	
	public void removeFlight(String fid) throws SQLException {
		
		String query = "delete from flight where fid = ?;";
		Connection con = DBConnection.getConnection();
		PreparedStatement pst = con.prepareStatement(query);
		pst.setString(1,fid);
		pst.executeUpdate();
		con.close();
		System.out.println("**********************************");
		System.out.println("Flight removed successful: "+fid);		
		System.out.println("**********************************");
	}
	
	public void updateFlightDetails(String fid, int capacity) {
		try {
			String query = "update flight set fcapacity = ? where fid = ?;";
			Connection con = DBConnection.getConnection();
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, capacity);
			pst.setString(2, fid);
			pst.executeUpdate();
			con.close();
			System.out.println("**********************************");
			System.out.println("Flight Updated successful: " + fid);
			System.out.println("**********************************");
		} catch (Exception e) {
			System.out.println("No flight available at id " + fid);
		}

	}

	public void getFlightsDetails() throws SQLException
	{
		Connection con = DBConnection.getConnection();
		String query = "select * from flight";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		
		while(rs.next()) {
			System.out.println("**************************************************************");
			System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+ (rs.getBoolean(3) ? "Business" : "Economy")+" "+rs.getInt(4));
		}
		System.out.println("**************************************************************");
		rs.close();
		con.close();
	}

	public void getAvailableSeats() throws SQLException {
		Connection con = DBConnection.getConnection();
		String query = "select fid ,fname ,fcapacity from flight";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		System.out.println("Flight id  Flignt name \t Available seats");
		while (rs.next()) {
			String fid = rs.getString(1);
			String q = "select count(pname) from booking where flightNo=?";
			PreparedStatement pst = con.prepareStatement(q);
			pst.setString(1, fid);
			ResultSet rst = pst.executeQuery();
			rst.next();
			System.out.println("-------------------------------------------");
			System.out.println(rs.getString(1) + " \t" + rs.getString(2) + " \t" + (rs.getInt(3) - rst.getInt(1)));
			rst.close();
			pst.close();
		}
		rs.close();
		st.close();
		con.close();
		System.out.println("-------------------------------------------");

	}


	public int getCapacity(String id) throws SQLException {
		String query = "Select fcapacity from flight where fid=" + id;
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		rs.next();
		int row = rs.getInt(1);
		rs.close();
		con.close();
		return row;
	}

}
