package AirlineReservation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	static final String url = "jdbc:mysql://localhost:3306/airline";
	static final String user = "root";
	static final String password = "";

	public static Connection getConnection() throws SQLException {
		Connection con = DriverManager.getConnection(url, user, password);
		return con;
	}
}
